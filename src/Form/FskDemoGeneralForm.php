<?php

namespace Drupal\fsk_demo\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Demonstrating a set of possible form elements.
 */
class FskDemoGeneralForm extends FormBase {

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritDoc}
   */
  public function __construct(PagerManagerInterface $pagerManager, MessengerInterface $messenger) {
    $this->pagerManager = $pagerManager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pager.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fsk_demo_general_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $title = $this->t('Text inputs');
    $description = '';
    $form['textfields'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['textfields']['normal'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default'),
      '#description' => $this->t('Default text field.'),
    ];
    $form['textfields']['required'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Required'),
      '#required' => TRUE,
      '#description' => $this->t('Default text field which is not optional.'),
    ];
    $form['textfields']['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('Default text field with a placeholder'),
      '#attributes' => [
        'placeholder' => $this->t('Placeholder'),
      ],
    ];
    $form['textfields']['input_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Input text'),
      '#default_value' => 'Input set',
      '#description' => $this->t('Default text field with some input'),
    ];
    $form['textfields']['disabled'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Disabled'),
      '#disabled' => TRUE,
      '#description' => $this->t('Default text field which is disabled.'),
    ];
    $form['textfields']['disabled_input'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Input text disabled'),
      '#disabled' => TRUE,
      '#default_value' => 'Change me, you must not',
      '#description' => $this->t('Default text field which is disabled and has some input.'),
    ];
    $form['textfields']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Input encrypted'),
      '#description' => $this->t('A password text field.'),
    ];
    $form['textfields']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#description' => $this->t('An email text field.'),
    ];
    $form['textfields']['number'] = [
      '#type' => 'email',
      '#title' => $this->t('Number'),
      '#description' => $this->t('A number text field.'),
    ];
    $form['textfields']['tel'] = [
      '#type' => 'tel',
      '#title' => $this->t('Telephone'),
      '#description' => $this->t('A telephone text field.'),
    ];
    $form['textfields']['text_server_validation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server side validated textfield'),
      '#description' => $this->t('A textfield which gets validated on server side.'),
    ];

    $title = $this->t('Textarea');
    $description = '';
    $form['textarea'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['textarea']['general'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Textarea'),
      '#default_value' => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed',
      '#description' => $this->t('Here you can add an additional description for each field, even if there is more than one line of text.'),
    ];
    $form['textarea']['error'] = $form['textarea']['general'];
    $form['textarea']['error']['#title'] = $this->t('Textarea that triggers error');
    $form['textarea']['wyswig'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Textarea'),
      '#default_value' => '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed</p>',
      '#description' => $this->t('Depends on CKeditor module to be turned on, and full_html text formats available for the user viewing this page.'),
    ];

    $title = $this->t('Checkboxes');
    $description = '';
    $form['checkboxes'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['checkboxes']['multiple'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Multiple'),
      '#options' => [
        'value_1' => $this->t('Checkbox One'),
        'value_2' => $this->t('Checkbox Two'),
        'value_3' => $this->t('Checkbox Three'),
        'value_4' => $this->t('Checkbox Four'),
        'value_5' => $this->t('Checkbox Five'),
      ],
      '#description' => $this->t('A checkbox group.'),
    ];
    $form['checkboxes']['multiple_error'] = $form['checkboxes']['multiple'];
    $form['checkboxes']['multiple_error']['#title'] = $this->t('Multiple checkbox which triggers error');
    $form['checkboxes']['single'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Single checkbox'),
      '#description' => $this->t('A single checkbox'),
    ];
    $form['checkboxes']['error'] = $form['checkboxes']['single'];
    $form['checkboxes']['error']['#title'] = $this->t('Single checkbox which triggers error');
    $form['checkboxes']['disabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disabled'),
      '#disabled' => TRUE,
      '#description' => $this->t('A single checkbox which is disabled.'),
    ];

    $title = $this->t('Radio buttons');
    $description = '';
    $form['radios'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['radios']['multiple'] = [
      '#type' => 'radios',
      '#title' => $this->t('Multiple'),
      '#options' => [
        'first_option' => $this->t('Radio option 1'),
        'second_option' => $this->t('Radio option 2'),
      ],
      '#description' => $this->t('Radio button group.'),
    ];
    $form['radios']['multiple_error'] = $form['radios']['multiple'];
    $form['radios']['multiple_error']['#title'] = $this->t('Radios multiple error');
    $form['radios']['normal'] = [
      '#type' => 'radio',
      '#title' => $this->t('Single'),
      '#description' => $this->t('A single radio button, its insanity, the one which you can undo..'),
    ];
    $form['radios']['error'] = $form['radios']['normal'];
    $form['radios']['error']['#title'] = $this->t('Radios single error');
    $form['radios']['error']['#description'] = $this->t('Once you click the single radio button, you can never go back...');
    $form['radios']['disabled'] = [
      '#type' => 'radio',
      '#title' => $this->t('Disabled'),
      '#disabled' => TRUE,
      '#description' => $this->t('A single radio button which is disabled.'),
    ];

    $title = $this->t('Select');
    $description = '';
    $form['select'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['select']['default'] = [
      '#type' => 'select',
      '#title' => $this->t('Default select'),
      '#options' => [
        'january' => $this->t('January'),
        'february' => $this->t('February'),
        'march' => $this->t('March'),
      ],
      '#description' => $this->t('A select list with few elements.'),
    ];
    $options = [];
    for ($i = 0; $i < 100; $i++) {
      $options[$i] = $i;
    }
    $form['select']['long_list'] = [
      '#type' => 'select',
      '#title' => $this->t('Long select'),
      '#options' => $options,
      '#description' => $this->t('A select list with lots of elements.'),
    ];
    $options[2] = $this->t('A needlessly large text inside a select list option to demo how it looks like when it has an odd option like this.');
    $form['select']['wide_list'] = [
      '#type' => 'select',
      '#title' => $this->t('Wide select'),
      '#options' => $options,
      '#description' => $this->t('A select list with wide text elements.'),
    ];
    $form['select']['datelist'] = [
      '#type' => 'datelist',
      '#title' => $this->t('Date'),
      '#description' => $this->t('A date selector in form of a list.'),
    ];

    $title = $this->t('Fieldset');
    $description = '';
    $form['fieldset'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['fieldset']['general_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fieldset Label'),
    ];
    $form['fieldset']['general_fieldset']['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail address'),
    ];
    $form['fieldset']['general_fieldset']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
    ];
    $form['fieldset']['general_fieldset']['file'] = [
      '#type' => 'file',
      '#title' => $this->t('File input'),
      '#description' => $this->t('An example description for file'),
    ];
    $form['fieldset']['general_fieldset']['checkbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check me out'),
    ];

    $title = $this->t('Details');
    $description = '';
    $form['details'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['details']['author'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Author'),
      '#group' => 'information',
    ];
    $form['details']['author']['name'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Name'),
    ];
    $form['details']['publication'] = [
      '#type' => 'details',
      '#title' => $this
        ->t('Publication'),
      '#group' => 'information',
    ];
    $form['details']['publication']['publisher'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Publisher'),
    ];

    $title = $this->t('Other elements');
    $description = '';
    $form['other'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['other']['date'] = [
      '#type' => 'date',
      '#title' => $this->t('Date'),
      '#description' => $this->t('A date selector.'),
    ];
    $form['other']['datetime'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Date Time'),
      '#description' => $this->t('A date selector along with time.'),
    ];
    $form['other']['range'] = [
      '#type' => 'range',
      '#title' => $this->t('Range'),
      '#description' => $this->t('A range selector.'),
    ];
    $form['other']['file'] = [
      '#type' => 'file',
      '#title' => $this->t('File input'),
      '#description' => $this->t('An example description for file'),
    ];
    $form['other']['managed_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Managed File input'),
      '#description' => $this->t('Manged file input render some extra info about file after upload'),
    ];

    $title = $this->t('Buttons');
    $description = '';
    $form['buttons'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['buttons']['actions']['action_1'] = [
      '#type' => 'submit',
      '#value' => $this->t('Primary button'),
    ];

    $title = $this->t('Dropdown');
    $description = '';
    $form['dropdown'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $form['dropdown']['dropbutton']['extra_actions'] = [
      '#type' => 'dropbutton',
      '#links' => [
        'simple_form' => [
          'title' => $this->t('Dropdown buttons'),
          'url' => Url::fromRoute('fsk_demo.fsk_demo_general_form'),
        ],
        'demo' => [
          'title' => $this
            ->t('Same page in another link'),
          'url' => Url::fromRoute('fsk_demo.fsk_demo_general_form'),
        ],
        'another_demo' => [
          'title' => $this
            ->t('Yet another link'),
          'url' => Url::fromRoute('fsk_demo.fsk_demo_general_form'),
        ],
      ],
    ];

    $title = $this->t('Paging');
    $description = '';
    $form['paging'] = [
      '#tree' => TRUE,
      '#type' => 'container',
      'header' => $this->addSectionHeader($title, $description),
    ];
    $this->pagerManager->createPager(100, 5);
    $form['paging']['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $form_state->setErrorByName('textfields][text_server_validation', $this->t('The "Server side validated textfield" will always be invalidated for all entries from server side.'));
    $form_state->setErrorByName('error', $this->t('The "Textarea that triggers error" will always be invalidated for all entries from server side.'));
    $form_state->setErrorByName('checkboxes][error', $this->t('Checkboxes error.'));
    $form_state->setErrorByName('checkboxes][multiple_error', $this->t('Multiple checkboxes error.'));
    $form_state->setErrorByName('radios][error', $this->t('Radios error.'));
    $form_state->setErrorByName('radios][multiple_error', $this->t('Multiple radio error.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Helper function to add a section header to form.
   */
  protected function addSectionHeader($title, $description) {
    return [
      '#type' => 'inline_template',
      '#template' => '<h2>{{ title }}</h2>' . ($description ? '<p>{{ description }}</p>' : ''),
      '#context' => [
        'title' => $title,
        'description' => $description,
      ],
    ];
  }

}
