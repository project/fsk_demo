<?php

namespace Drupal\fsk_demo\Form;

use Drupal\user\Form\UserLoginForm;

/**
 * A user login form.
 *
 * @package Drupal\fsk_demo\Form
 *
 * @phpstan-ignore-next-line
 */
class FskUserLoginForm extends UserLoginForm {

}
