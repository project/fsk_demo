<?php

declare(strict_types=1);

namespace Drupal\fsk_demo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Factorial Starter Kit Demo routes.
 */
final class FskDemoStatusMessagesController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly MessengerInterface $messenger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('messenger'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {
    $url = \Drupal\Core\Url::fromUri('https://api.drupal.org/api/drupal/core%21modules%21system%21templates%21status-messages.html.twig/10', ['absolute' => TRUE]);
    $link = \Drupal::linkGenerator()->generate('Drupal API/10/status-messages.html.twig', $url);
    $this->messenger->addStatus('A "status" message (this was printed first).');
    $this->messenger->addStatus('Another "status" message.');
    $this->messenger->addMessage('A "default" message.');
    $this->messenger->addMessage('Another "default" message.');
    $this->messenger->addWarning('A "warning" message.');
    $this->messenger->addError('An "error" message.');
    $this->messenger->addStatus('A "status" message which was printed after some errors and warning were printed would still appear along with other status messages..');
    $this->messenger->addStatus($this->t('A "status" message can also have links like this @link.', ['@link' => $link]));
    $build['heading'] = [
      '#type' => 'item',
      '#markup' => $this->t('Fsk Demo status messages.'),
    ];
    return $build;
  }

}
